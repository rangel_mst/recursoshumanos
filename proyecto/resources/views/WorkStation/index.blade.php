@extends('home.main')


@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Proyecto Reingenieria</h1>

            <div class="containerDepartments">
                <h2>Lista de Estaciones de Trabajo</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Salary</th>
                        <th>Descripcion</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($workStations as $workStation)
                        <tr>
                            <td>{{$workStation->idworkStation}}</td>
                            <td><a href="{{action('WorkStationsController@edit',$workStation->idworkStation)}}">{{$workStation->name}}</a></td>
                            <td>{{$workStation->salary}}</td>
                            <td>{{$workStation->description}}</td>

                            <td>
                                {!! Form::open([
                                          'method' => 'DELETE',
                                          'route' => ['WorkStation.destroy', $workStation->idworkStation]
                                      ]) !!}
                                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop