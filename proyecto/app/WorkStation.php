<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkStation extends Model
{
    protected $table = 'workStation';
    protected $primaryKey  = 'idworkStation';
}
