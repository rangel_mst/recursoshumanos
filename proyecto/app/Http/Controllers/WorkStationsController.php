<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\WorkStation;

class WorkStationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $workStations = WorkStation::all();

        return view('WorkStation.index',compact('workStations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::pluck('name','idDepartments')->all();

        return view('WorkStation.create',compact('departments'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $workstation = new WorkStation;

        $workstation->name = $request->inputName;
        $workstation->salary = $request->inputSalary;
        $workstation->description = $request->inputDescription;
        $workstation->Department_idDepartments = $request->inputDepartment;
        $workstation->timestamps = false;
        $workstation->save();

        return redirect('/WorkStation');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $departments = Department::pluck('name','idDepartments')->all();
        $workstation = WorkStation::where('idworkStation','=',$id)->first();

        return view('WorkStation.edit',compact('workstation','departments'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $workstation = WorkStation::where('idworkStation','=',$id)->first();

        $workstation->name = $request->name;
        $workstation->salary = $request->salary;
        $workstation->description = $request->description;
        $workstation->timestamps = false;
        $workstation->save();

        //Session::flash('message', 'Successfully updated nerd!');
        //return view('Departments.create');

        return redirect('/WorkStation');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $workstation = Department::find($id);
        $workstation ->delete();

        return redirect('/WorkStation');
    }
}
