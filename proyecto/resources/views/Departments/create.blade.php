@extends('home.main')


@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Crear Departamentos</h1>

            {!! Form::open(['method'=>'POST','action'=>'DepartmentsController@store']) !!}

            <div class="form-group">
                {!! Form::label('inputName','Name:') !!}
                {!! Form::text('inputName',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('inputDescription','Descripción:') !!}
                {!! Form::textarea('inputDescription',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Crear',['class'=>'btn btn-primary']) !!}
            </div>

        {!! Form::close() !!}

        <!-- END PAGE HEADER-->
            <div class="note note-info">
                <p> ejemplo de notas dentro de las vistas </p>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop