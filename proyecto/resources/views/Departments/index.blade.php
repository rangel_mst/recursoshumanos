@extends('home.main')


@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Proyecto Reingenieria</h1>

            <div class="containerDepartments">
                <h2>Lista de Departamentos</h2>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Descripcion</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($departments as $department)
                        <tr>
                            <td>{{$department->idDepartments}}</td>
                            <td><a href="{{action('DepartmentsController@edit',$department->idDepartments)}}">{{$department->name}}</a></td>
                            <td>{{$department->description}}</td>

                            <td>
                                {!! Form::open([
                                          'method' => 'DELETE',
                                          'route' => ['Department.destroy', $department->idDepartments]
                                      ]) !!}
                                {!! Form::submit('Eliminar', ['class' => 'btn btn-danger']) !!}
                                {!! Form::close() !!}
                            </td>

                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop