@extends('home.main')


@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <h1 class="page-title"> Editar Departamentos</h1>

            {!! Form::model($department,['method'=>'PATCH','action'=>['DepartmentsController@update',$department->idDepartments]]) !!}

            <div class="form-group">
                {!! Form::label('name','Name:') !!}
                {!! Form::text('name',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::label('description','Descripción:') !!}
                {!! Form::textarea('description',null,['class'=>'form-control']) !!}
            </div>

            <div class="form-group">
                {!! Form::submit('Editar',['class'=>'btn btn-primary']) !!}
            </div>

            {!! Form::close() !!}

        </div>
        <!-- END CONTENT BODY -->
    </div>
@stop